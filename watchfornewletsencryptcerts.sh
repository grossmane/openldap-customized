#!/bin/bash

while true
do
    inotifywait -e modify /container/service/slapd/assets/certs/cert.pem
    echo "Certificate changed. Reload ldap"
    sleep 5
    kill -INT `cat /run/slapd/slapd.pid`
done
