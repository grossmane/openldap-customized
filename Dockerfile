FROM osixia/openldap-backup:1.3.0

RUN apt-get -qq update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qq inotify-tools vim less \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

COPY ./start-ldap-customized.sh /usr/local/bin/
COPY ./watchfornewletsencryptcerts.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/*

ENTRYPOINT /usr/local/bin/start-ldap-customized.sh
